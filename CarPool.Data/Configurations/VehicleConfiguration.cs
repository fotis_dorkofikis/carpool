﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder
                .HasKey(v => v.PlateNumber);

            builder
                .Property(v => v.NumberOfSeats)
                .IsRequired();

            builder
                .Property(v => v.Model)
                .HasMaxLength(50);

            builder
                .Property(v => v.Brand)
                .HasMaxLength(50);

            builder
                .HasOne(v => v.User)
                .WithMany(u => u.Vehicles)
                .HasForeignKey(v => v.UserId);

            builder
                .ToTable("Vehicles");
        }
    }
}