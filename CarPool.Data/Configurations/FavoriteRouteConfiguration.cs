﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class FavoriteRouteConfiguration : IEntityTypeConfiguration<FavoriteRoute>
    {
        public void Configure(EntityTypeBuilder<FavoriteRoute> builder)
        {
            builder
                .HasKey(fr => new {CustomPointID = fr.RouteId, fr.UserId});

            builder
                .Property(fr => fr.Comments)
                .IsRequired();

            builder
                .HasOne(fr => fr.User)
                .WithMany(u => u.FavoriteRoutes)
                .HasForeignKey(fr => fr.UserId);

            builder
                .HasOne(fr => fr.Route)
                .WithMany(r => r.FavoriteRoutes)
                .HasForeignKey(fr => fr.RouteId);

            builder
                .ToTable("FavoriteRoutes");
        }
    }
}