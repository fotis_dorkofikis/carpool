﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class RoutePointConfiguration : IEntityTypeConfiguration<RoutePoint>
    {
        public void Configure(EntityTypeBuilder<RoutePoint> builder)
        {
            builder
                .HasKey(r => new {CustomPointID = r.CustomPointId, r.RouteId});

            /*builder
                .HasOne(rp => rp.CustomPoint)
                .WithMany(cp => cp.RoutePoints)
                .HasForeignKey(rp => rp.CustomPointId);
            
            builder
                .HasOne(rp => rp.Route)
                .WithMany(r => r.RoutePoints)
                .HasForeignKey(rp => rp.RouteId);*/

            builder
                .ToTable("RoutePoints");
        }
    }
}