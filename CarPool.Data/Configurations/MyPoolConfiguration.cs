﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class MyPoolConfiguration : IEntityTypeConfiguration<MyPool>
    {
        public void Configure(EntityTypeBuilder<MyPool> builder)
        {
            builder
                .HasKey(mp => mp.Id);

            builder
                .Property(mp => mp.Id)
                .UseIdentityColumn();

            builder
                .Property(mp => mp.Description)
                .IsRequired();

            builder
                .Property(mp => mp.Start)
                .IsRequired();

            builder
                .HasOne(mp => mp.User)
                .WithMany(u => u.MyPools)
                .HasForeignKey(mp => mp.UserId);

            builder
                .HasOne(mp => mp.Vehicle)
                .WithMany(v => v.MyPools)
                .HasForeignKey(mp => mp.VehicleId);

            builder
                .HasOne(mp => mp.Route)
                .WithMany(r => r.MyPools)
                .HasForeignKey(mp => mp.RouteId);

            builder
                .ToTable("MyPool");
        }
    }
}