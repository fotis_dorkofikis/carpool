﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class FriendConfiguration : IEntityTypeConfiguration<Friend>
    {
        public void Configure(EntityTypeBuilder<Friend> builder)
        {
            builder
                .HasKey(f => new {f.UserId, f.FriendId});

            builder
                .HasOne(f => f.User)
                .WithMany(u => u.Friends)
                .HasForeignKey(f => f.UserId);

            builder
                .HasOne(f => f.UserData)
                .WithMany(u => u.Friends)
                .HasForeignKey(f => f.FriendId)
                .OnDelete(DeleteBehavior.NoAction);


            builder
                .ToTable("Friends");
        }
    }
}