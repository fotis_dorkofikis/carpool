﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class CustomPointConfiguration : IEntityTypeConfiguration<CustomPoint>
    {
        public void Configure(EntityTypeBuilder<CustomPoint> builder)
        {
            builder
                .HasKey(p => p.Id);

            builder
                .Property(p => p.Id)
                .UseIdentityColumn();

            builder
                .Property(p => p.Latitude)
                .IsRequired();

            builder
                .Property(p => p.Longitude)
                .IsRequired();

            builder
                .ToTable("CustomPoints");
        }
    }
}