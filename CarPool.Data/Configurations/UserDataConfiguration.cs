﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class UserDataConfiguration : IEntityTypeConfiguration<UserData>
    {
        public void Configure(EntityTypeBuilder<UserData> builder)
        {
            builder
                .HasKey(ud => ud.UserId);

            builder
                .Property(ud => ud.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(ud => ud.LastName)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(ud => ud.Email)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(ud => ud.Phone)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .HasOne(ud => ud.User)
                .WithOne(u => u.UserData)
                .HasForeignKey<UserData>(ud => ud.UserId);

            builder
                .ToTable("UserData");
        }
    }
}