﻿using CarPool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarPool.Data.Configurations
{
    public class UserClaimConfiguration : IEntityTypeConfiguration<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            builder
                .HasKey(uc => uc.UserId);

            builder
                .Property(uc => uc.Role)
                .IsRequired()
                .HasMaxLength(20);

            builder
                .HasOne(uc => uc.User)
                .WithOne(u => u.UserClaim)
                .HasForeignKey<UserClaim>(uc => uc.UserId);

            builder
                .ToTable("UserClaims");
        }
    }
}