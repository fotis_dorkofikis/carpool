﻿using CarPool.Core.Models;
using CarPool.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data
{
    public class CarPoolContext : DbContext
    {
        public CarPoolContext(DbContextOptions<CarPoolContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserData> UserData { get; set; }
        public DbSet<UserClaim> UserClaim { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Route> Routes { get; set; }

        public DbSet<CustomPoint> CustomPoints { get; set; }

        public DbSet<RoutePoint> RoutePoints { get; set; }
        public DbSet<MyPool> MyPools { get; set; }
        public DbSet<FavoriteRoute> FavoriteRoutes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new UserConfiguration());
            builder
                .ApplyConfiguration(new UserDataConfiguration());
            builder
                .ApplyConfiguration(new UserClaimConfiguration());
            builder
                .ApplyConfiguration(new FriendConfiguration());
            builder
                .ApplyConfiguration(new VehicleConfiguration());
            builder
                .ApplyConfiguration(new CustomPointConfiguration());
            builder
                .ApplyConfiguration(new RouteConfiguration());
            builder
                .ApplyConfiguration(new RoutePointConfiguration());
            builder
                .ApplyConfiguration(new MyPoolConfiguration());
            builder
                .ApplyConfiguration(new FavoriteRouteConfiguration());
        }
    }
}