﻿using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Repositories;
using CarPool.Data.Repositories;

namespace CarPool.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CarPoolContext _context;
        private CustomPointRepository _customPointRepository;
        private FavoriteRouteRepository _favoriteRouteRepository;
        private FriendRepository _friendRepository;
        private MyPoolRepository _myPoolRepository;
        private RoutePointRepository _routePointRepository;
        private RouteRepository _routeRepository;
        private UserDataRepository _userDataRepository;
        private UserRepository _userRepository;
        private VehicleRepository _vehicleRepository;

        public UnitOfWork(CarPoolContext context)
        {
            _context = context;
        }

        public IUserRepository Users => _userRepository = _userRepository ?? new UserRepository(_context);

        public IUserDataRepository UsersData =>
            _userDataRepository = _userDataRepository ?? new UserDataRepository(_context);

        public ICustomPointRepository CustomPoints =>
            _customPointRepository = _customPointRepository ?? new CustomPointRepository(_context);

        public IMyPoolRepository MyPools =>
            _myPoolRepository = _myPoolRepository ?? new MyPoolRepository(_context);

        public IRouteRepository Routes => _routeRepository = _routeRepository ?? new RouteRepository(_context);

        public IRoutePointRepository RoutePoints =>
            _routePointRepository = _routePointRepository ?? new RoutePointRepository(_context);

        public IFavoriteRouteRepository FavoriteRoutes => _favoriteRouteRepository =
            _favoriteRouteRepository ?? new FavoriteRouteRepository(_context);

        public IFriendRepository Friends => _friendRepository = _friendRepository ?? new FriendRepository(_context);

        public IVehicleRepository Vehicles =>
            _vehicleRepository = _vehicleRepository ?? new VehicleRepository(_context);

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}