﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class FavoriteRouteRepository : Repository<FavoriteRoute>, IFavoriteRouteRepository
    {
        public FavoriteRouteRepository(DbContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<FavoriteRoute>> GetAllFavoriteRoutesAsync()
        {
            return await CarPoolContext.FavoriteRoutes.ToListAsync();
        }

        public async Task<List<FavoriteRoute>> GetFavoriteRouteByUserIdAsync(int userId)
        {
            return await CarPoolContext.FavoriteRoutes.Where(fr => fr.UserId == userId).ToListAsync();
        }

        public async Task<List<FavoriteRoute>> GetFavoriteRouteByRouteIdAsync(int routeId)
        {
            return await CarPoolContext.FavoriteRoutes.Where(fr => fr.RouteId == routeId).ToListAsync();
        }

        public async Task<FavoriteRoute> GetFavoriteRouteByUserIdAndRouteIdAsync(int userId, int routeId)
        {
            return await CarPoolContext.FavoriteRoutes.FirstOrDefaultAsync(fr =>
                fr.UserId == userId && fr.RouteId == routeId);
        }
    }
}