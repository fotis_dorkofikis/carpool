﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class CustomPointRepository : Repository<CustomPoint>, ICustomPointRepository
    {
        public CustomPointRepository(DbContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<CustomPoint>> GetAllCustomPointsAsync()
        {
            return await CarPoolContext.CustomPoints.ToListAsync();
        }

        public async Task<CustomPoint> GetCustomPointByIdAsync(int id)
        {
            return await CarPoolContext.CustomPoints.FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}