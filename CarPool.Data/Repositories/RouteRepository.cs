﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class RouteRepository : Repository<Route>, IRouteRepository
    {
        public RouteRepository(DbContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<IEnumerable<Route>> GetAllRouteAsync()
        {
            return await CarPoolContext.Routes
                .Include(r => r.RoutePoints)
                .ThenInclude(rp => rp.CustomPoint)
                .ToListAsync();
            //return await CarPoolContext.Routes.ToListAsync();
        }

        public async Task<Route> GetRouteByIdAsync(int id)
        {
            return await CarPoolContext.Routes
                .Include(r => r.RoutePoints)
                .ThenInclude(rp => rp.CustomPoint)
                .FirstOrDefaultAsync(r => r.Id == id);
            //return await CarPoolContext.Routes.FirstOrDefaultAsync(r => r.Id == id);
        }
    }
}