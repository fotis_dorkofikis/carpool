﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class RoutePointRepository : Repository<RoutePoint>, IRoutePointRepository
    {
        public RoutePointRepository(DbContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<RoutePoint>> GetAllRoutePointsAsync()
        {
            return await CarPoolContext.RoutePoints.ToListAsync();
        }

        public async Task<List<RoutePoint>> GetRoutePointByRouteIdAsync(int routeId)
        {
            return await CarPoolContext.RoutePoints.Where(rp => rp.RouteId == routeId).ToListAsync();
        }

        public async Task<List<RoutePoint>> GetAllRoutePointsExtraAsync()
        {
            return await CarPoolContext.RoutePoints.Include(rp => rp.CustomPoint).ToListAsync();
        }

        public async Task<List<RoutePoint>> GetRoutePointByRouteExtraIdAsync(int routeId)
        {
            return await CarPoolContext.RoutePoints.Where(rp => rp.RouteId == routeId).Include(rp => rp.CustomPoint)
                .ToListAsync();
        }
    }
}