﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CarPoolContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<User>> GetAllUsersAsync()
        {
            return await CarPoolContext.Users.ToListAsync();
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            return await CarPoolContext.Users
                .Include(u => u.UserData)
                .SingleOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            return await CarPoolContext.Users
                .Include(u => u.UserClaim)
                .Include(u => u.UserData)
                .SingleOrDefaultAsync(u => u.UserName.Equals(username));
        }
    }
}