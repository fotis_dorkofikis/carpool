﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class MyPoolRepository : Repository<MyPool>, IMyPoolRepository
    {
        public MyPoolRepository(DbContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<MyPool>> GetCarPoolsAsync()
        {
            return await CarPoolContext.MyPools.ToListAsync();
        }

        public async Task<MyPool> GetCarPoolByIdAsync(int id)
        {
            return await CarPoolContext.MyPools.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<List<MyPool>> GetMyPoolsByUserIdAsync(int userId)
        {
            return await CarPoolContext.MyPools.Where(mp => mp.UserId == userId).ToListAsync();
        }

        public async Task<List<MyPool>> GetMyPoolsByRouteIdAsync(int routeId)
        {
            return await CarPoolContext.MyPools.Where(mp => mp.RouteId == routeId).ToListAsync();
        }
    }
}