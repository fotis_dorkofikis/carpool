﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class FriendRepository : Repository<Friend>, IFriendRepository
    {
        public FriendRepository(CarPoolContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<IEnumerable<Friend>> GetAllFriendsAsync()
        {
            return await CarPoolContext.Friends.ToListAsync();
        }

        public async Task<IEnumerable<Friend>> GetFriendsByUserIdAsync(int userId)
        {
            return await CarPoolContext.Friends.Where(f => f.UserId == userId).ToListAsync();
        }

        public async Task<Friend> GetFriendByUserIdAndFriendId(int userId, int friendId)
        {
            return await CarPoolContext.Friends.FirstOrDefaultAsync(f => f.UserId == userId && f.FriendId == friendId);
        }
    }
}