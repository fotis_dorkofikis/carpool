﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(CarPoolContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<Vehicle>> GetAllVehiclesAsync()
        {
            return await CarPoolContext.Vehicles.ToListAsync();
        }

        public async Task<Vehicle> GetVehicleByPlateNumberAsync(string plateNumber)
        {
            return await CarPoolContext.Vehicles.FirstOrDefaultAsync(v => v.PlateNumber.Equals(plateNumber));
        }

        public async Task<List<Vehicle>> GetAllVehiclesByUserIdAsync(int userId)
        {
            return await CarPoolContext.Vehicles.Where(v => v.UserId == userId).ToListAsync();
        }
    }
}