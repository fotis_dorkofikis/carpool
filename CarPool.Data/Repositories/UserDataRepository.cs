﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;
using CarPool.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarPool.Data.Repositories
{
    public class UserDataRepository : Repository<UserData>, IUserDataRepository
    {
        public UserDataRepository(CarPoolContext context) : base(context)
        {
        }

        private CarPoolContext CarPoolContext => Context as CarPoolContext;

        public async Task<List<UserData>> GetAllUsersDataAsync()
        {
            return await CarPoolContext.UserData.ToListAsync();
        }

        public async Task<UserData> GetUserDataByUserIdAsync(int userId)
        {
            return await CarPoolContext.UserData
                .SingleOrDefaultAsync(u => u.User.Id == userId);
        }
    }
}