﻿using System;
using System.Threading.Tasks;
using CarPool.Core.Repositories;

namespace CarPool.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IUserDataRepository UsersData { get; }
        IRouteRepository Routes { get; }
        ICustomPointRepository CustomPoints { get; }
        IRoutePointRepository RoutePoints { get; }
        IMyPoolRepository MyPools { get; }
        IFavoriteRouteRepository FavoriteRoutes { get; }
        IFriendRepository Friends { get; }
        IVehicleRepository Vehicles { get; }

        Task<int> CompleteAsync();
    }
}