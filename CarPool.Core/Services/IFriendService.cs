﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IFriendService
    {
        Task<IEnumerable<Friend>> GetAllFriends();
        Task<IEnumerable<Friend>> GetFriendsByUserId(int userId);
        Task<Friend> GetFriendByUserIdAndFriendId(int userId, int friendId);
        Task<Friend> SaveFriend(Friend friend);
        Task UpdateFriend(Friend friendToBeUpdated, Friend friend);
        Task DeleteFriend(Friend friend);
    }
}