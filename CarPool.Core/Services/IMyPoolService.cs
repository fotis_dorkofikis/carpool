﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IMyPoolService
    {
        Task<List<MyPool>> GetAllMyPools();
        Task<MyPool> GetMyPoolById(int id);
        Task<List<MyPool>> GetMyPoolsByCustomPoints(CustomPoint startPoint, CustomPoint endPoint);
        Task<List<MyPool>> GetMyPoolsByUserId(int userId);
        Task<List<MyPool>> GetMyPoolsByRouteId(int routeId);
        Task<MyPool> SaveMyPool(MyPool myPool);
        Task UpdateMyPool(MyPool myPoolToBeUpdated, MyPool myPool);
        Task DeleteMyPool(MyPool myPool);
    }
}