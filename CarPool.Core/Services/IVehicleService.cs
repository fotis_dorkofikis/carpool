﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IVehicleService
    {
        Task<List<Vehicle>> GetAllVehicles();
        Task<List<Vehicle>> GetAllVehicles(int userId);
        Task<Vehicle> GetVehicleByPlateNumber(string plateNumber);
        Task<List<Vehicle>> GetAllVehiclesByUserId(int userId);
        Task<Vehicle> SaveVehicle(Vehicle vehicle);
        Task UpdateVehicle(Vehicle vehicleToBeUpdated, Vehicle vehicle);
        Task DeleteVehicle(Vehicle vehicle);
    }
}