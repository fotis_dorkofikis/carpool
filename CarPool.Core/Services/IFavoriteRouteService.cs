﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IFavoriteRouteService
    {
        Task<List<FavoriteRoute>> GetAllFavoriteRoutes();
        Task<List<FavoriteRoute>> GetAllFavoriteRoutes(int userId);
        Task<List<FavoriteRoute>> GetFavoriteRouteByUserId(int userId);
        Task<List<FavoriteRoute>> GetFavoriteRouteByRouteId(int routeId);
        Task<FavoriteRoute> GetFavoriteRouteByUserIdAndRouteId(int userId, int routeId);
        Task<FavoriteRoute> SaveFavoriteRoute(FavoriteRoute favoriteRoute);
        Task UpdateFavoriteRoute(FavoriteRoute favoriteRouteToBeUpdated, FavoriteRoute favoriteRoute);
        Task DeleteFavoriteRoute(FavoriteRoute favoriteRoute);
    }
}