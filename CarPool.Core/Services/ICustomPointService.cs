﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface ICustomPointService
    {
        Task<IEnumerable<CustomPoint>> GetAllCustomPoints();
        Task<CustomPoint> GetCustomPointById(int id);
        Task<CustomPoint> SaveCustomPoint(CustomPoint customPoint);
        Task<IEnumerable<CustomPoint>> SaveCustomPoints(List<CustomPoint> customPoints);
        Task UpdateCustomPoint(CustomPoint customPointToBeUpdated, CustomPoint customPoint);
        Task DeleteCustomPoint(CustomPoint customPoint);
    }
}