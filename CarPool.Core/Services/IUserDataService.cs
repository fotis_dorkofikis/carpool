﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IUserDataService
    {
        Task<List<UserData>> GetAllUsersData();
        Task<UserData> GetUserDataByUserId(int userId);
        Task<UserData> SaveUserData(UserData userData);
        Task UpdateUserData(UserData userDataToBeUpdated, UserData userData);
        Task DeleteUserData(UserData userData);
    }
}