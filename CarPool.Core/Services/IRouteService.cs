﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IRouteService
    {
        Task<IEnumerable<Route>> GetAllRoutes();
        Task<Route> GetRouteById(int id);
        Task<Route> SaveRoute(Route route);
        Task UpdateRoute(Route routeToBeUpdated, Route route);
        Task DeleteRoute(Route route);
    }
}