﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Services
{
    public interface IRoutePointService
    {
        Task<IEnumerable<RoutePoint>> SaveRoutePoints(List<RoutePoint> routePoints);
    }
}