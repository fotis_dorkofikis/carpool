﻿using System.Collections.Generic;
using CarPool.Core.ApiModels;

namespace CarPool.Core.Models
{
    public class ApiRoute
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public IEnumerable<ApiPoint> Points { get; set; }
    }
}