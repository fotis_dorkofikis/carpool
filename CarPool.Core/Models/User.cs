﻿using System.Collections.Generic;

namespace CarPool.Core.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public UserData UserData { get; set; }
        public UserClaim UserClaim { get; set; }
        public ICollection<Friend> Friends { get; set; }
        public ICollection<Vehicle> Vehicles { get; set; }
        public ICollection<FavoriteRoute> FavoriteRoutes { get; set; }
        public ICollection<MyPool> MyPools { get; set; }
    }
}