﻿using System.Collections.Generic;

namespace CarPool.Core.Models
{
    public class Route
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public ICollection<RoutePoint> RoutePoints { get; set; }
        public IEnumerable<MyPool> MyPools { get; set; }
        public IEnumerable<FavoriteRoute> FavoriteRoutes { get; set; }
    }
}