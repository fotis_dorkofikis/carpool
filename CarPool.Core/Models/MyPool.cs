﻿using System;

namespace CarPool.Core.Models
{
    public class MyPool
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public int FreeSeats { get; set; }
        public int UserId { get; set; }
        public int RouteId { get; set; }
        public string VehicleId { get; set; }

        public User User { get; set; }

        public Route Route { get; set; }

       public Vehicle Vehicle { get; set; }
    }
}