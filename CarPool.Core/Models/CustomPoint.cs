﻿﻿using System.Collections.Generic;
 using System.Globalization;
 using System.Linq;
 using NetTopologySuite.Geometries;

 namespace CarPool.Core.Models
{
    public class CustomPoint
    {
        public string Description { get; set; }
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Point Point{ get; set; }
        public bool IsPublic { get; set; }
        
        public ICollection<RoutePoint> RoutePoints { get; set; }
        
        public override bool Equals(object obj)
        {
            var item = obj as CustomPoint;

            if (item == null)
            {
                return false;
            }

            return (this.Latitude.Equals(item.Latitude) && this.Longitude.Equals(item.Longitude));
        }
        public override int GetHashCode()
        {
            return Latitude.ToString(CultureInfo.InvariantCulture).Concat(Longitude.ToString(CultureInfo.InvariantCulture)).GetHashCode();
        }
    }
}