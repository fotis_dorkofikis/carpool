﻿using System.Collections.Generic;

namespace CarPool.Core.Models
{
    public class UserData
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public long Phone { get; set; }

        public User User { get; set; }
        public ICollection<Friend> Friends { get; set; }
    }
}