﻿namespace CarPool.Core.Models
{
    public class RoutePoint
    {
        public int CustomPointId { get; set; }
        public int RouteId { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }

        public CustomPoint CustomPoint { get; set; }
        public Route Route { get; set; }
    }
}