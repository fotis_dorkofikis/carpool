﻿namespace CarPool.Core.Models
{
    public class FavoriteRoute
    {
        public int RouteId { get; set; }
        public int UserId { get; set; }
        public string Comments { get; set; }

         public Route Route { get; set; }

         public User User { get; set; }

    }
}