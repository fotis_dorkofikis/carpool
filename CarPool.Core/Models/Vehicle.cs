﻿using System.Collections.Generic;

namespace CarPool.Core.Models
{
    public class Vehicle
    {
        public string PlateNumber { get; set; }
        public int NumberOfSeats { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public int UserId { get; set; }

        public User User { get; set; }

        public IEnumerable<MyPool> MyPools { get; set; }
    }
}