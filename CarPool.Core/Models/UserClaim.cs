﻿namespace CarPool.Core.Models
{
    public class UserClaim
    {
        public int UserId { get; set; }
        public string Role { get; set; }

        public User User { get; set; }
    }
}