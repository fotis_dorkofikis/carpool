﻿namespace CarPool.Core.Models
{
    public class Friend
    {
        public int UserId { get; set; }
        public int FriendId { get; set; }
        public string NickName { get; set; }
        public string Comment { get; set; }

        public UserData UserData { get; set; }
        public User User { get; set; }
    }
}