﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IFavoriteRouteRepository : IRepository<FavoriteRoute>
    {
        Task<List<FavoriteRoute>> GetAllFavoriteRoutesAsync();
        Task<List<FavoriteRoute>> GetFavoriteRouteByUserIdAsync(int userId);
        Task<List<FavoriteRoute>> GetFavoriteRouteByRouteIdAsync(int routeId);
        Task<FavoriteRoute> GetFavoriteRouteByUserIdAndRouteIdAsync(int userId, int routeId);
    }
}