﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IUserDataRepository : IRepository<UserData>
    {
        Task<List<UserData>> GetAllUsersDataAsync();
        Task<UserData> GetUserDataByUserIdAsync(int userId);
    }
}