﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(int id);
        Task<User> GetUserByUsernameAsync(string username);
    }
}