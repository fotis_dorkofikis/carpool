﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IFriendRepository : IRepository<Friend>
    {
        Task<IEnumerable<Friend>> GetAllFriendsAsync();
        Task<IEnumerable<Friend>> GetFriendsByUserIdAsync(int userId);
        Task<Friend> GetFriendByUserIdAndFriendId(int userId, int friendId);
    }
}