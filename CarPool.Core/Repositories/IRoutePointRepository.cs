﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IRoutePointRepository : IRepository<RoutePoint>

    {
        Task<List<RoutePoint>> GetAllRoutePointsAsync();
        Task<List<RoutePoint>> GetRoutePointByRouteIdAsync(int routeId);
    }
}