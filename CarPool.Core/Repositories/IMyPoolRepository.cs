﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IMyPoolRepository : IRepository<MyPool>
    {
        Task<List<MyPool>> GetCarPoolsAsync();
        Task<MyPool> GetCarPoolByIdAsync(int id);
        Task<List<MyPool>> GetMyPoolsByUserIdAsync(int userId);
        Task<List<MyPool>> GetMyPoolsByRouteIdAsync(int routeId);
    }
}