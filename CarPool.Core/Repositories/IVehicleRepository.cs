﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        Task<List<Vehicle>> GetAllVehiclesAsync();
        Task<Vehicle> GetVehicleByPlateNumberAsync(string plateNumber);
        Task<List<Vehicle>> GetAllVehiclesByUserIdAsync(int userId);
    }
}