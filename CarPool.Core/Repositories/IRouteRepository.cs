﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface IRouteRepository : IRepository<Route>
    {
        Task<IEnumerable<Route>> GetAllRouteAsync();
        Task<Route> GetRouteByIdAsync(int id);
    }
}