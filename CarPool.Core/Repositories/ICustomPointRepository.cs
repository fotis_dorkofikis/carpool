﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core.Models;

namespace CarPool.Core.Repositories
{
    public interface ICustomPointRepository : IRepository<CustomPoint>
    {
        Task<List<CustomPoint>> GetAllCustomPointsAsync();
        Task<CustomPoint> GetCustomPointByIdAsync(int id);
    }
}