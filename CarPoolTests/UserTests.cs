using System;
using System.Linq;
using CarPool.Core.Models;
using CarPool.Data;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CarPoolTests
{
    public class UserTests
    {
        [Fact]
        public async void AddUser()
        {
            var options = new DbContextOptionsBuilder<CarPoolContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var unitOfWork = new UnitOfWork(new CarPoolContext(options)))
            {
                await unitOfWork.Users.AddAsync(new User
                {
                    Id = 1,
                    UserName = "Fotis",
                    Password = "123"
                });
                await unitOfWork.CompleteAsync();
            }

            using (var unitOfWork = new UnitOfWork(new CarPoolContext(options)))
            {
                var users = unitOfWork.Users.GetAllUsersAsync().Result;
                Assert.Single(users);
                Assert.Equal(1, users.FirstOrDefault()?.Id);
                Assert.Equal("Fotis", users.FirstOrDefault()?.UserName);
                Assert.Equal("123", users.FirstOrDefault()?.Password);
            }
        }

        [Fact]
        public async void AddUsers()
        {
            var options = new DbContextOptionsBuilder<CarPoolContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var unitOfWork = new UnitOfWork(new CarPoolContext(options)))
            {
                await unitOfWork.Users.AddAsync(new User
                {
                    Id = 1,
                    UserName = "Fotis",
                    Password = "123"
                });
                await unitOfWork.Users.AddAsync(new User
                {
                    Id = 2,
                    UserName = "Kostas",
                    Password = "345"
                });
                await unitOfWork.CompleteAsync();
            }

            using (var unitOfWork = new UnitOfWork(new CarPoolContext(options)))
            {
                var users = unitOfWork.Users.GetAllUsersAsync().Result;
                Assert.NotEmpty(users);
                Assert.Equal(2, users.Count);
            }
        }
    }
}