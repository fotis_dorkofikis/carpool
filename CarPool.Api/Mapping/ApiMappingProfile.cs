﻿﻿using AutoMapper;
using CarPool.Api.Resources;
 using CarPool.Core.ApiModels;
 using CarPool.Core.Models;

namespace CarPool.Api.Mapping
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<User, UserResource>().ReverseMap();
            CreateMap<UserData, UserDataResource>().ReverseMap();
            CreateMap<CustomPoint, CustomPointResource>().ReverseMap();
            CreateMap<Route, RouteResource>().ReverseMap();
            CreateMap<FavoriteRoute, FavoriteRouteResource>().ReverseMap();
            CreateMap<Friend, FriendResource>().ReverseMap();
            CreateMap<ApiRoute, RouteResource>().ReverseMap();
            CreateMap<Vehicle, VehicleResource>().ReverseMap();
            CreateMap<MyPool, MyPoolResource>().ReverseMap();
            
            CreateMap<ApiRoutePointResource, RoutePoint>()
                .ForMember(rp => rp.CustomPoint,
                    opts => opts.MapFrom(
                        src => new CustomPoint()
                        {
                            Id = src.Id,
                            Description = src.Description,
                            Latitude = src.Latitude,
                            Longitude = src.Longitude,
                            IsPublic = src.IsPublic
                        }));
            CreateMap<RoutePoint, ApiRoutePointResource>()
                .IncludeMembers(rp => rp.CustomPoint);
            CreateMap<CustomPoint, ApiRoutePointResource>();
        }
    }
}