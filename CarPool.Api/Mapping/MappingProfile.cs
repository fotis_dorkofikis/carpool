﻿using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.ApiModels;
using CarPool.Core.Models;

namespace CarPool.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserResource>().ReverseMap();
            CreateMap<UserData, UserDataResource>().ReverseMap();
            CreateMap<CustomPoint, CustomPointResource>().ReverseMap();
            CreateMap<ApiPoint, RoutePointResource>().ReverseMap();
            CreateMap<Route, RouteResource>().ReverseMap();
            CreateMap<FavoriteRoute, FavoriteRouteResource>().ReverseMap();
            CreateMap<Friend, FriendResource>().ReverseMap();
            CreateMap<ApiRoute, RouteResource>().ReverseMap();
            CreateMap<Vehicle, VehicleResource>().ReverseMap();
            CreateMap<MyPool, MyPoolResource>().ReverseMap();
        }
    }
}