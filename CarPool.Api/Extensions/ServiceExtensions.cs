﻿using System;
using System.Text;
using CarPool.Core;
using CarPool.Core.Services;
using CarPool.Data;
using CarPool.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CarPool.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureServiceInterface(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserDataService, UserDataService>();
            services.AddTransient<IRouteService, RouteService>();
            services.AddTransient<ICustomPointService, CustomPointService>();
            services.AddTransient<IRoutePointService, RoutePointService>();
            services.AddTransient<IFavoriteRouteService, FavoriteRouteService>();
            services.AddTransient<IFriendService, FriendService>();
            services.AddTransient<IVehicleService, VehicleService>();
            services.AddTransient<IMyPoolService, MyPoolService>();
        }

        public static void ConfigureAuthorization(this IServiceCollection services)
        {
            var sharedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ThisIsTheSecretKey"));
            services.AddAuthentication(opt =>
                {
                    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = "http://localhost:5000",
                        //ValidAudience = "http://localhost:5000",//to do check that id is in the list of existing userID
                        IssuerSigningKey = sharedKey
                    };
                });
        }

        public static void ConfigureDB(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CarPoolContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("Default"),
                        b => b.UseNetTopologySuite()));
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options => { });
        }
    }
}