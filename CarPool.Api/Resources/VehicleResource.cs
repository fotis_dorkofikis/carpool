﻿namespace CarPool.Api.Resources
{
    public class VehicleResource
    {
        public string PlateNumber { get; set; }
        public int NumberOfSeats { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public int UserId { get; set; }
    }
}