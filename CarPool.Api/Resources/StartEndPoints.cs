﻿namespace CarPool.Api.Resources
{
    public class StartEndPoints
    {
        public CustomPointResource StartPoint { get; set; }
        public CustomPointResource EndPoint { get; set; }
    }
}