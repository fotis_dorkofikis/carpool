﻿namespace CarPool.Api.Resources
{
    public class UserResource
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public UserDataResource UserDataResource { get; set; }
    }
}