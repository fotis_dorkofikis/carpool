﻿namespace CarPool.Api.Resources
{
    public class AppUserResource
    {
        public int Id { get; set; }
        public UserResource UserResource { get; set; }
        public UserDataResource UserDataResource { get; set; }
    }
}