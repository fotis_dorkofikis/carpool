﻿using NetTopologySuite.Geometries;

namespace CarPool.Api.Resources
{
    public class ApiRoutePointResource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        //public Coordinate Coordinates{ get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public bool IsPublic { get; set; }
    }
}