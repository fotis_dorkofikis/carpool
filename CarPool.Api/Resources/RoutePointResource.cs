﻿namespace CarPool.Api.Resources
{
    public class RoutePointResource
    {
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public PointResource ApiPoint { get; set; }
    }
}