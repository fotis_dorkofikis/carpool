﻿namespace CarPool.Api.Resources
{
    public class FriendResource
    {
        public int UserId { get; set; }
        public int FriendId { get; set; }
        public string NickName { get; set; }
        public string Comment { get; set; }
    }
}