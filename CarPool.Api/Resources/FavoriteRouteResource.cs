﻿namespace CarPool.Api.Resources
{
    public class FavoriteRouteResource
    {
        public int RouteId { get; set; }
        public int UserId { get; set; }
        public string Comments { get; set; }
    }
}