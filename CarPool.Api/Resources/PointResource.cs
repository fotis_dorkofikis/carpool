﻿using System;
using System.Drawing;

namespace CarPool.Api.Resources
{
    public class PointResource
    {
        public int Id { get; set; }

        public string Description { get; set; }
        //public Coordinate Coordinates{ get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool IsPublic { get; set; }
    }
}