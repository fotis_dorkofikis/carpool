﻿using System.Collections.Generic;

namespace CarPool.Api.Resources
{
    public class RouteResource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public ICollection<ApiRoutePointResource> RoutePoints { get; set; }
    }
}