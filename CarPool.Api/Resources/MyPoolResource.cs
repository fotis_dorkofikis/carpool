﻿using System;

namespace CarPool.Api.Resources
{
    public class MyPoolResource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public int FreeSeats { get; set; }
        public int UserId { get; set; }
        public int RouteId { get; set; }
        public string VehicleId { get; set; }
    }
}