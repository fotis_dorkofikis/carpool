﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendsController : ControllerBase
    {
        private readonly IFriendService _friendService;
        private readonly IMapper _mapper;

        public FriendsController(IFriendService friendService, IMapper mapper)
        {
            _friendService = friendService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<FriendResource>>> GetAllFriends()
        {
            var friends = await _friendService.GetAllFriends();
            var friedResources = _mapper.Map<IEnumerable<Friend>, IEnumerable<FriendResource>>(friends);
            return Ok(friedResources);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<FriendResource>>> GetFriendsByUserId(int userId)
        {
            var friends = await _friendService.GetFriendsByUserId(userId);
            var friendResources = _mapper.Map<IEnumerable<Friend>, IEnumerable<FriendResource>>(friends);

            return Ok(friendResources);
        }

        [HttpGet("{userId},{friendId}")]
        public async Task<ActionResult<FriendResource>> GetFriendByUserIdAndFriendId(int userId, int friendId)
        {
            var friend = await _friendService.GetFriendByUserIdAndFriendId(userId, friendId);
            var friendResource = _mapper.Map<Friend, FriendResource>(friend);

            return Ok(friendResource);
        }

        [HttpPost("")]
        public async Task<ActionResult<FriendResource>> CreateCustomPoint([FromBody] FriendResource saveFriendResource)
        {
            var friendToCreate = _mapper.Map<FriendResource, Friend>(saveFriendResource);

            var newFriend = await _friendService.SaveFriend(friendToCreate);

            var friend = await _friendService.GetFriendByUserIdAndFriendId(newFriend.UserId, newFriend.FriendId);

            var friendResource = _mapper.Map<Friend, FriendResource>(friend);

            return Ok(friendResource);
        }

        [HttpPut("{userId},{friendId}")]
        public async Task<ActionResult<FriendResource>> UpdateCustomPoint(int userId, int friendId,
            [FromBody] FriendResource saveFriendResource)
        {
            var friendToBeUpdated = await _friendService.GetFriendByUserIdAndFriendId(userId, friendId);

            if (friendToBeUpdated == null)
                return NotFound();

            var friend = _mapper.Map<FriendResource, Friend>(saveFriendResource);

            await _friendService.UpdateFriend(friendToBeUpdated, friend);

            var updatedFriend = await _friendService.GetFriendByUserIdAndFriendId(userId, friendId);
            var updatedFriendResource = _mapper.Map<Friend, FriendResource>(updatedFriend);

            return Ok(updatedFriendResource);
        }

        [HttpDelete("{userId},{friendId}")]
        public async Task<IActionResult> DeleteCustomPoint(int userId, int friendId)
        {
            if (userId == 0 || friendId == 0)
                return BadRequest();

            var friend = await _friendService.GetFriendByUserIdAndFriendId(userId, friendId);

            if (friend == null)
                return NotFound();

            await _friendService.DeleteFriend(friend);

            return NoContent();
        }
    }
}