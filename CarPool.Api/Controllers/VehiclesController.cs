﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;

        public VehiclesController(IVehicleService vehicleService, IMapper mapper)
        {
            _vehicleService = vehicleService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<VehicleResource>>> GetAllVehicles()
        {
            //List<Vehicle> vehicles;
            //Checking that Api Caller has the right to update specific UserInformation
            /*if (User.IsInRole("Admin"))
            {
                vehicles = await _vehicleService.GetAllVehicles();
            }
            else
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                vehicles = await _vehicleService.GetAllVehicles(userId);
            }*/
            var vehicles = await _vehicleService.GetAllVehicles();
            var vehicleResources =
                _mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleResource>>(vehicles);
            return Ok(vehicleResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleResource>> GetVehicleById(string plateNumber)
        {
            var vehicle = await _vehicleService.GetVehicleByPlateNumber(plateNumber);
            //Checking that Api Caller has the right to get specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                if (userId != vehicle.UserId)
                    return Unauthorized();
            }*/
            var vehicleResource = _mapper.Map<Vehicle, VehicleResource>(vehicle);

            return Ok(vehicleResource);
        }

        [HttpPost("")]
        public async Task<ActionResult<VehicleResource>> CreateVehicle(
            [FromBody] VehicleResource saveVehicleResource)
        {
            var vehicleToCreate = _mapper.Map<VehicleResource, Vehicle>(saveVehicleResource);

            var newVehicle = await _vehicleService.SaveVehicle(vehicleToCreate);

            var vehicle = await _vehicleService.GetVehicleByPlateNumber(newVehicle.PlateNumber);

            var vehicleResource = _mapper.Map<Vehicle, VehicleResource>(vehicle);

            return Ok(vehicleResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<VehicleResource>> UpdateRoute(string plateNumber,
            [FromBody] VehicleResource saveVehicleResource)
        {
            //Checking that Api Caller has the right to edit specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                if (userId != saveVehicleResource.UserId)
                    return Unauthorized();
            }*/
            var vehicleToBeUpdated = await _vehicleService.GetVehicleByPlateNumber(plateNumber);

            if (vehicleToBeUpdated == null)
                return NotFound();

            var vehicleResource = _mapper.Map<VehicleResource, Vehicle>(saveVehicleResource);

            await _vehicleService.UpdateVehicle(vehicleToBeUpdated, vehicleResource);

            var updatedVehicle = await _vehicleService.GetVehicleByPlateNumber(plateNumber);
            var updatedVehicleResource = _mapper.Map<Vehicle, VehicleResource>(updatedVehicle);

            return Ok(updatedVehicleResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehicle(string plateNumber)
        {
            if (plateNumber == null)
                return BadRequest();

            var vehicle = await _vehicleService.GetVehicleByPlateNumber(plateNumber);

            if (vehicle == null)
                return NotFound();
            
            //Checking that Api Caller has the right to delete specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                if (userId != vehicle.UserId)
                    return Unauthorized();
            }*/

            await _vehicleService.DeleteVehicle(vehicle);

            return NoContent();
        }
    }
}