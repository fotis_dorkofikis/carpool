﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteRoutesController : Controller
    {
        private readonly IFavoriteRouteService _favoriteRouteService;
        private readonly IMapper _mapper;

        public FavoriteRoutesController(IFavoriteRouteService favoriteRouteService, IMapper mapper)
        {
            _favoriteRouteService = favoriteRouteService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<FavoriteRouteResource>>> GetAllFavoriteRoutes()
        {
            //List<FavoriteRoute> favoriteRoutes;
            //Checking that Api Caller has the right to update specific UserInformation
            /*if (User.IsInRole("Admin"))
            {
                favoriteRoutes = await _favoriteRouteService.GetAllFavoriteRoutes();
            }
            else
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                favoriteRoutes = await _favoriteRouteService.GetAllFavoriteRoutes(userId);
            }*/
            var favoriteRoutes = await _favoriteRouteService.GetAllFavoriteRoutes();
            var favoriteRouteResources =
                _mapper.Map<IEnumerable<FavoriteRoute>, IEnumerable<FavoriteRouteResource>>(favoriteRoutes);
            return Ok(favoriteRouteResources);
        }

        [HttpGet("{userId},{routeId}")]
        public async Task<ActionResult<FavoriteRouteResource>> GetFavoriteRouteByUserIdAndRouteId(int userId,
            int routeId)
        {
            var favoriteRoute = await _favoriteRouteService.GetFavoriteRouteByUserIdAndRouteId(userId, routeId);
            //Checking that Api Caller has the right to get specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var id))
                    return BadRequest();
                if (id != favoriteRoute.UserId)
                    return Unauthorized();
            }*/
            var favoriteRouteResource = _mapper.Map<FavoriteRoute, FavoriteRouteResource>(favoriteRoute);

            return Ok(favoriteRouteResource);
        }

        [HttpGet("FoUser/{userId}")]
        public async Task<ActionResult<IEnumerable<FavoriteRouteResource>>> GetFavoriteRouteByUserId(int userId)
        {
            var favoriteRoutes = await _favoriteRouteService.GetFavoriteRouteByUserId(userId);
            var favoriteRouteResources =
                _mapper.Map<IEnumerable<FavoriteRoute>, IEnumerable<FavoriteRouteResource>>(favoriteRoutes);

            return Ok(favoriteRouteResources);
        }

        [HttpGet("{routeId}")]
        public async Task<ActionResult<IEnumerable<FavoriteRouteResource>>> GetFavoriteRouteByRouteId(int routeId)
        {
            var favoriteRoutes = await _favoriteRouteService.GetFavoriteRouteByRouteId(routeId);
            var favoriteRouteResources =
                _mapper.Map<IEnumerable<FavoriteRoute>, IEnumerable<FavoriteRouteResource>>(favoriteRoutes);

            return Ok(favoriteRouteResources);
        }

        [HttpPost("")]
        public async Task<ActionResult<FavoriteRouteResource>> CreateFavoriteRoute(
            [FromBody] FavoriteRouteResource saveFavoriteRouteResource)
        {
            var favoriteRouteToCreate = _mapper.Map<FavoriteRouteResource, FavoriteRoute>(saveFavoriteRouteResource);

            var newFavoriteRoute = await _favoriteRouteService.SaveFavoriteRoute(favoriteRouteToCreate);

            var favoriteRoute =
                await _favoriteRouteService.GetFavoriteRouteByUserIdAndRouteId(newFavoriteRoute.UserId,
                    newFavoriteRoute.RouteId);

            var favoriteRouteResource = _mapper.Map<FavoriteRoute, FavoriteRouteResource>(favoriteRoute);

            return Ok(favoriteRouteResource);
        }

        [HttpPut("{userId},{routeId}")]
        public async Task<ActionResult<FavoriteRouteResource>> UpdateUser(int userId, int routeId,
            [FromBody] FavoriteRouteResource saveFavoriteRouteResource)
        {
            var favoriteRouteToBeUpdated =
                await _favoriteRouteService.GetFavoriteRouteByUserIdAndRouteId(userId, routeId);

            if (favoriteRouteToBeUpdated == null)
                return NotFound();

            var favoriteRoute = _mapper.Map<FavoriteRouteResource, FavoriteRoute>(saveFavoriteRouteResource);

            await _favoriteRouteService.UpdateFavoriteRoute(favoriteRouteToBeUpdated, favoriteRoute);

            var updatedFavoriteRoute = await _favoriteRouteService.GetFavoriteRouteByUserIdAndRouteId(userId, routeId);
            var updatedFavoriteRouteResource = _mapper.Map<FavoriteRoute, FavoriteRouteResource>(updatedFavoriteRoute);

            return Ok(updatedFavoriteRouteResource);
        }

        [HttpDelete("{userId},{routeId}")]
        public async Task<IActionResult> DeleteUser(int userId, int routeId)
        {
            if (userId == 0 || routeId == 0)
                return BadRequest();

            var favoriteRoute = await _favoriteRouteService.GetFavoriteRouteByUserIdAndRouteId(userId, routeId);

            if (favoriteRoute == null)
                return NotFound();

            await _favoriteRouteService.DeleteFavoriteRoute(favoriteRoute);

            return NoContent();
        }
    }
}