﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly IRouteService _routeService;
        private readonly ICustomPointService _customPointService;
        private readonly IMapper _mapper;

        public RoutesController(IRouteService routeService, ICustomPointService customPointService, IMapper mapper)
        {
            _routeService = routeService;
            _customPointService = customPointService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<RouteResource>>> GetAllRoutes()
        {
            var routes = await _routeService.GetAllRoutes();
            var routeResources =  _mapper.Map<List<RouteResource>>(routes);
            return Ok(routeResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RouteResource>> GetRouteById(int id)
        {
            var route = await _routeService.GetRouteById(id);
            var routeResource = _mapper.Map<RouteResource>(route);
            return Ok(routeResource);
        }

        [HttpPost("")]
        public async Task<ActionResult<RouteResource>> CreateRoute(
            [FromBody] RouteResource saveRouteResource)
        {
            var routeToCreate = _mapper.Map<Route>(saveRouteResource);
            foreach (var routePoint in routeToCreate.RoutePoints)
            {
                if (routePoint.CustomPoint.Id != 0)
                    routePoint.CustomPoint = await _customPointService.GetCustomPointById(routePoint.CustomPoint.Id);
            }
            var newRoute = await _routeService.SaveRoute(routeToCreate);
            var route = await _routeService.GetRouteById(newRoute.Id);
            var routeResource = _mapper.Map<RouteResource>(route);
            return Ok(routeResource);
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRoute(int id)
        {
            //Checking that Api Caller has the right to update specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
            }
            if (id == 0)
                return BadRequest();
            var route = await _routeService.GetRouteById(id);
            if (route == null)
                return NotFound();*/
            var route = await _routeService.GetRouteById(id);
            await _routeService.DeleteRoute(route);
            return NoContent();
        }
    }
}