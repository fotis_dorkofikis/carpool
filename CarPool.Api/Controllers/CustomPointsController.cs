﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
//    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomPointsController : ControllerBase
    {
        private readonly ICustomPointService _customPointService;
        private readonly IMapper _mapper;

        public CustomPointsController(ICustomPointService customPointService, IMapper mapper)
        {
            _customPointService = customPointService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<CustomPointResource>>> GetAllCustomPoints()
        {
            var customPoints = await _customPointService.GetAllCustomPoints();
            var customPointResources =
                _mapper.Map<IEnumerable<CustomPoint>, IEnumerable<CustomPointResource>>(customPoints);
            return Ok(customPointResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomPointResource>> GetCustomPointById(int id)
        {
            var customPoint = await _customPointService.GetCustomPointById(id);
            var customPointResource = _mapper.Map<CustomPoint, CustomPointResource>(customPoint);

            return Ok(customPointResource);
        }

        [HttpPost("")]
        public async Task<ActionResult<CustomPointResource>> CreateCustomPoint(
            [FromBody] CustomPointResource saveCustomPointResource)
        {
            var customPointToCreate = _mapper.Map<CustomPointResource, CustomPoint>(saveCustomPointResource);

            var newCustomPoint = await _customPointService.SaveCustomPoint(customPointToCreate);

            var customPoint = await _customPointService.GetCustomPointById(newCustomPoint.Id);

            var customPointResource = _mapper.Map<CustomPoint, CustomPointResource>(customPoint);

            return Ok(customPointResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<CustomPointResource>> UpdateCustomPoint(int id,
            [FromBody] CustomPointResource saveCustomPointResource)
        {
            //Checking that Api Caller has the right to update specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
            }*/
            var customPointToBeUpdated = await _customPointService.GetCustomPointById(id);

            if (customPointToBeUpdated == null)
                return NotFound();

            var customPoint = _mapper.Map<CustomPointResource, CustomPoint>(saveCustomPointResource);

            await _customPointService.UpdateCustomPoint(customPointToBeUpdated, customPoint);

            var updatedCustomPoint = await _customPointService.GetCustomPointById(id);
            var updatedCustomPointResource = _mapper.Map<CustomPoint, CustomPointResource>(updatedCustomPoint);

            return Ok(updatedCustomPointResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomPoint(int id)
        {
            //Checking that Api Caller has the right to delete specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
            }*/
            
            if (id == 0)
                return BadRequest();

            var customPoint = await _customPointService.GetCustomPointById(id);

            if (customPoint == null)
                return NotFound();

            await _customPointService.DeleteCustomPoint(customPoint);

            return NoContent();
        }
    }
}