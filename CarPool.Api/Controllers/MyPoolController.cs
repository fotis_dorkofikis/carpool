﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MyPoolController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMyPoolService _myPoolService;

        public MyPoolController(IMyPoolService myPoolService, IMapper mapper)
        {
            _myPoolService = myPoolService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<MyPoolResource>>> GetAllMyPools()
        {
            var myPools = await _myPoolService.GetAllMyPools();
            var myPoolsResources = _mapper.Map<IEnumerable<MyPool>, IEnumerable<MyPoolResource>>(myPools);
            return Ok(myPoolsResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MyPoolResource>> GetMyPoolById(int id)
        {
            var myPool = await _myPoolService.GetMyPoolById(id);
            var myPoolResource = _mapper.Map<MyPool, MyPoolResource>(myPool);

            return Ok(myPoolResource);
        }
        
        [HttpGet("/GetPoolsByStartEndPoints")]
        public async Task<ActionResult<IEnumerable<MyPoolResource>>> GetPoolsByStartEndPoints([FromBody] StartEndPoints points )
        {
            var startPoint = _mapper.Map<CustomPoint>(points.StartPoint);
            var endPoint = _mapper.Map<CustomPoint>(points.EndPoint);
            var myPools = await _myPoolService.GetMyPoolsByCustomPoints(startPoint,endPoint);
            var myPoolsResources = _mapper.Map<IEnumerable<MyPool>, IEnumerable<MyPoolResource>>(myPools);
            return Ok(myPoolsResources);
        }

        [HttpPost("")]
        public async Task<ActionResult<MyPoolResource>> CreateMyPool([FromBody] MyPoolResource saveMyPoolResource)
        {
            var myPoolToCreate = _mapper.Map<MyPoolResource, MyPool>(saveMyPoolResource);

            var newMyPool = await _myPoolService.SaveMyPool(myPoolToCreate);

            var myPool = await _myPoolService.GetMyPoolById(newMyPool.Id);

            var userResource = _mapper.Map<MyPool, MyPoolResource>(myPool);

            return Ok(userResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<MyPoolResource>> UpdateUser(int id, [FromBody] MyPoolResource saveMyPoolResource)
        {
            //Checking that Api Caller has the right to update specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                if (userId != saveMyPoolResource.UserId)
                    return Unauthorized();
            }*/
            var myPoolToBeUpdated = await _myPoolService.GetMyPoolById(id);

            if (myPoolToBeUpdated == null)
                return NotFound();

            var myPool = _mapper.Map<MyPoolResource, MyPool>(saveMyPoolResource);

            await _myPoolService.UpdateMyPool(myPoolToBeUpdated, myPool);

            var updatedMyPool = await _myPoolService.GetMyPoolById(id);
            var updatedMyPoolResource = _mapper.Map<MyPool, MyPoolResource>(updatedMyPool);

            return Ok(updatedMyPoolResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMyPool(int id)
        {
            if (id == 0)
                return BadRequest();

            var myPool = await _myPoolService.GetMyPoolById(id);

            if (myPool == null)
                return NotFound();
            
            //Checking that Api Caller has the right to delete specific UserInformation
            /*if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var userId))
                    return BadRequest();
                if (userId != myPool.UserId)
                    return Unauthorized();
            }*/

            await _myPoolService.DeleteMyPool(myPool);

            return NoContent();
        }
    }
}