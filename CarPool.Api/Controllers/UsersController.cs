﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarPool.Api.Resources;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPool.Api.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserDataService _userDataService;
        private readonly IUserService _userService;

        public UsersController(IMapper mapper, IUserService userService, IUserDataService userDataService)
        {
            _mapper = mapper;
            _userService = userService;
            _userDataService = userDataService;
        }

        //[AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<string>> GetAppUserDataByUserId([FromBody] LoginResource loginResource)
        {
            var authenticatedUser = await _userService.Authenticate(loginResource.Username, loginResource.Password);
            if (authenticatedUser == null)
                return BadRequest(new {message = "Username or password is incorrect"});

            return Ok(new TokenConfig().GetLoginResponse(authenticatedUser));
        }

        //[AllowAnonymous]
        [HttpPost("Register")]
        public async Task<ActionResult<UserResource>> CreateUser([FromBody] UserResource saveUserResource)
        {
            var user = await _userService.GetUserByUsername(saveUserResource.Username);
            if (user != null) return BadRequest("User Already Exists");

            saveUserResource.Password = await _userService.HashPassword(saveUserResource.Password);
            var userToCreate = _mapper.Map<User>(saveUserResource);
            userToCreate.UserClaim = new UserClaim {Role = "User"};
            var newUser = await _userService.SaveUser(userToCreate);
            var userResource = _mapper.Map<UserResource>(newUser);
            userResource.Password = null;

            return Ok(userResource);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<UserResource>> GetAppUserData(int userId)
        {
            //Checking that Api Caller has the right to update specific UserInformation
            if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var id))
                    return BadRequest();
                if (userId != id)
                    return Unauthorized();
            }

            var user = await _userService.GetUserById(userId);
            if (user == null)
                return BadRequest();

            var userResource = _mapper.Map<UserResource>(user);
            userResource.Password = null;
            return Ok(userResource);
        }

        [HttpPut("Update")]
        public async Task<ActionResult<UserResource>> UpdateUserData(int userId,
            [FromBody] UserResource saveUserResource)
        {
            /*//Checking that Api Caller has the right to update specific UserInformation
            if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var id))
                    return BadRequest();
                if (userId != id)
                    return Unauthorized();
            }*/

            var userToBeUpdated = await _userService.GetUserById(userId);
            if (userToBeUpdated == null)
                return BadRequest();

            saveUserResource.Password = PasswordHasher.Hash(saveUserResource.Password);
            var user = _mapper.Map<User>(saveUserResource);
            await _userService.UpdateUser(userToBeUpdated, user);
            var updatedUser = await _userService.GetUserById(userId);
            var updatedUserResource = _mapper.Map<UserResource>(updatedUser);
            updatedUserResource.Password = null;
            return Ok(updatedUserResource);
        }

        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            /*//Checking that Api Caller has the right to update specific UserInformation
            if (!User.IsInRole("Admin"))
            {
                if (!int.TryParse(User.Identity.Name, out var id))
                    return BadRequest();
                if (userId != id)
                    return Unauthorized();
            }*/

            var userTobeDeleted = await _userService.GetUserById(userId);
            if (userTobeDeleted == null)
                return NotFound();

            await _userService.DeleteUser(userTobeDeleted);
            return NoContent();
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserResource>>> GetAppUsers()
        {
            var users = await _userService.GetAllUsers();
            var userResources = _mapper.Map<IEnumerable<UserResource>>(users);
            return Ok(userResources);
        }
    }
}