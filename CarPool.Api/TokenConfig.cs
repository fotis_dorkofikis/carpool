﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CarPool.Core.Models;
using Microsoft.IdentityModel.Tokens;

namespace CarPool.Api
{
    public class TokenConfig
    {
        private static readonly SymmetricSecurityKey SharedKey =
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ThisIsTheSecretKey"));

        private readonly SigningCredentials _signinCredentials =
            new SigningCredentials(SharedKey, SecurityAlgorithms.HmacSha256);

        public dynamic GetLoginResponse(User authenticatedUser)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, authenticatedUser.UserClaim.UserId.ToString()),
                new Claim(ClaimTypes.Role, authenticatedUser.UserClaim.Role)
            };

            var tokeOptions = new JwtSecurityToken(
                "http://localhost:5000",
                //audience: "http://localhost:5000",
                claims: claims, //Todo authenticatedUser.Claims to return IEnumerable
                expires: DateTime.Now.AddHours(1),
                signingCredentials: _signinCredentials
            );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

            return new
            {
                authenticatedUser.Id,
                Token = tokenString
            };
        }
    }
}