﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class RoutePointService : IRoutePointService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoutePointService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RoutePoint>> SaveRoutePoints(List<RoutePoint> routePoints)
        {
            await _unitOfWork.RoutePoints.AddRangeAsync(routePoints);
            await _unitOfWork.CompleteAsync();
            return routePoints;
        }
    }
}