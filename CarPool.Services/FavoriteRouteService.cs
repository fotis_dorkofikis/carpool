﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class FavoriteRouteService : IFavoriteRouteService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FavoriteRouteService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<FavoriteRoute>> GetAllFavoriteRoutes()
        {
            return await _unitOfWork.FavoriteRoutes.GetAllFavoriteRoutesAsync();
        }
        
        public async Task<List<FavoriteRoute>> GetAllFavoriteRoutes(int userId)
        {
            var favoriteRoutes = await _unitOfWork.FavoriteRoutes.GetAllFavoriteRoutesAsync();
            return favoriteRoutes.Where(fr => fr.UserId == userId).ToList();
        }

        public async Task<List<FavoriteRoute>> GetFavoriteRouteByUserId(int userId)
        {
            return await _unitOfWork.FavoriteRoutes.GetFavoriteRouteByUserIdAsync(userId);
        }

        public async Task<List<FavoriteRoute>> GetFavoriteRouteByRouteId(int routeId)
        {
            return await _unitOfWork.FavoriteRoutes.GetFavoriteRouteByRouteIdAsync(routeId);
        }

        public async Task<FavoriteRoute> GetFavoriteRouteByUserIdAndRouteId(int userId, int routeId)
        {
            return await _unitOfWork.FavoriteRoutes.GetFavoriteRouteByUserIdAndRouteIdAsync(userId, routeId);
        }

        public async Task<FavoriteRoute> SaveFavoriteRoute(FavoriteRoute favoriteRoute)
        {
            await _unitOfWork.FavoriteRoutes.AddAsync(favoriteRoute);
            await _unitOfWork.CompleteAsync();
            return favoriteRoute;
        }

        public async Task UpdateFavoriteRoute(FavoriteRoute favoriteRouteToBeUpdated, FavoriteRoute favoriteRoute)
        {
            favoriteRouteToBeUpdated.Comments = favoriteRoute.Comments;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteFavoriteRoute(FavoriteRoute favoriteRoute)
        {
            _unitOfWork.FavoriteRoutes.Remove(favoriteRoute);
            await _unitOfWork.CompleteAsync();
        }
    }
}