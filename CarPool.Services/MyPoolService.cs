﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class MyPoolService : IMyPoolService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MyPoolService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<MyPool>> GetAllMyPools()
        {
            return await _unitOfWork.MyPools.GetCarPoolsAsync();
        }

        public async Task<MyPool> GetMyPoolById(int id)
        {
            return await _unitOfWork.MyPools.GetCarPoolByIdAsync(id);
        }

        public async Task<List<MyPool>> GetMyPoolsByCustomPoints(CustomPoint startPoint, CustomPoint endpoint)
        {
            var myCarpools = await _unitOfWork.MyPools.GetCarPoolsAsync();
            var result = new List<MyPool>();
            foreach (var carpool in myCarpools) {
                foreach (var routePoint in carpool.Route.RoutePoints)
                {
                    if (routePoint.IsStart)
                    {
                        if (startPoint.Id != 0)
                        {
                            if (routePoint.CustomPoint.Id != startPoint.Id)
                            {
                                continue;
                            }
                        }
                        else if (!routePoint.CustomPoint.Equals(startPoint))
                        {
                            continue;
                        }
                    }

                    if (routePoint.IsEnd)
                    {
                        if (endpoint.Id != 0)
                        {
                            if (routePoint.CustomPoint.Id != endpoint.Id)
                            {
                                continue;
                            }
                        }
                        else if (!routePoint.CustomPoint.Equals(endpoint))
                        {
                            continue;
                        }
                    }
                    myCarpools.Add(carpool);
                }
            }
            return myCarpools;
        }

        public async Task<List<MyPool>> GetMyPoolsByUserId(int userId)
        {
            return await _unitOfWork.MyPools.GetMyPoolsByUserIdAsync(userId);
        }

        public async Task<List<MyPool>> GetMyPoolsByRouteId(int routeId)
        {
            return await _unitOfWork.MyPools.GetMyPoolsByRouteIdAsync(routeId);
        }

        public async Task<MyPool> SaveMyPool(MyPool myPool)
        {
            await _unitOfWork.MyPools.AddAsync(myPool);
            await _unitOfWork.CompleteAsync();
            return myPool;
        }

        public async Task UpdateMyPool(MyPool myPoolToBeUpdated, MyPool myPool)
        {
            myPoolToBeUpdated.Description = myPool.Description;
            myPoolToBeUpdated.FreeSeats = myPool.FreeSeats;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteMyPool(MyPool myPool)
        {
            _unitOfWork.MyPools.Remove(myPool);
            await _unitOfWork.CompleteAsync();
        }
    }
}