﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class RouteService : IRouteService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RouteService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Route>> GetAllRoutes()
        {
            return await _unitOfWork.Routes.GetAllRouteAsync();
        }

        public async Task<Route> GetRouteById(int id)
        {
            return await _unitOfWork.Routes.GetRouteByIdAsync(id);
        }

        public async Task<Route> SaveRoute(Route route)
        {
            await _unitOfWork.Routes.AddAsync(route);
            await _unitOfWork.CompleteAsync();
            return route;
        }

        public async Task UpdateRoute(Route routeToBeUpdated, Route route)
        {
            routeToBeUpdated.Description = route.Description;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteRoute(Route route)
        {
            _unitOfWork.Routes.Remove(route);
            await _unitOfWork.CompleteAsync();
        }
    }
}