﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class CustomPointService : ICustomPointService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomPointService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<CustomPoint>> GetAllCustomPoints()
        {
            return await _unitOfWork.CustomPoints.GetAllCustomPointsAsync();
        }

        public async Task<CustomPoint> GetCustomPointById(int id)
        {
            return await _unitOfWork.CustomPoints.GetCustomPointByIdAsync(id);
        }

        public async Task<CustomPoint> SaveCustomPoint(CustomPoint customPoint)
        {
            await _unitOfWork.CustomPoints.AddAsync(customPoint);
            await _unitOfWork.CompleteAsync();
            return customPoint;
        }

        public async Task<IEnumerable<CustomPoint>> SaveCustomPoints(List<CustomPoint> customPoints)
        {
            await _unitOfWork.CustomPoints.AddRangeAsync(customPoints);
            await _unitOfWork.CompleteAsync();
            return customPoints;
        }

        public async Task UpdateCustomPoint(CustomPoint customPointToBeUpdated, CustomPoint customPoint)
        {
            customPointToBeUpdated.Latitude = customPoint.Latitude;
            customPointToBeUpdated.Longitude = customPoint.Longitude;
            customPointToBeUpdated.Description = customPoint.Description;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteCustomPoint(CustomPoint customPoint)
        {
            _unitOfWork.CustomPoints.Remove(customPoint);
            await _unitOfWork.CompleteAsync();
        }
    }
}