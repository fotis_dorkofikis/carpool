﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = await _unitOfWork.Users.GetUserByUsernameAsync(username);
            if (user == null)
                return null;
            var isTheSame = await PasswordHasher.CheckAsych(user.Password, password);
            return isTheSame ? user : null;
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _unitOfWork.Users.GetAllUsersAsync();
        }

        public async Task<User> GetUserById(int id)
        {
            return await _unitOfWork.Users.GetUserByIdAsync(id);
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await _unitOfWork.Users.GetUserByUsernameAsync(username);
        }

        public async Task<User> SaveUser(User user)
        {
            await _unitOfWork.Users.AddAsync(user);
            await _unitOfWork.CompleteAsync();
            return user;
        }

        public async Task UpdateUser(User userToBeUpdated, User user)
        {
            userToBeUpdated.UserName = user.UserName;
            userToBeUpdated.Password = user.Password;
            userToBeUpdated.UserData = user.UserData;

            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteUser(User user)
        {
            _unitOfWork.Users.Remove(user);
            await _unitOfWork.CompleteAsync();
        }

        public async Task<string> HashPassword(string password)
        {
            return await PasswordHasher.HashAsync(password);
        }
    }
}