﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public VehicleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Vehicle>> GetAllVehicles()
        {
            return await _unitOfWork.Vehicles.GetAllVehiclesAsync();
        }
        
        public async Task<List<Vehicle>> GetAllVehicles(int userId)
        {
            var vehicles = await _unitOfWork.Vehicles.GetAllVehiclesAsync();
            return vehicles.Where(v => v.UserId == userId).ToList();
        }

        public async Task<Vehicle> GetVehicleByPlateNumber(string plateNumber)
        {
            return await _unitOfWork.Vehicles.GetVehicleByPlateNumberAsync(plateNumber);
        }

        public async Task<List<Vehicle>> GetAllVehiclesByUserId(int userId)
        {
            return await _unitOfWork.Vehicles.GetAllVehiclesByUserIdAsync(userId);
        }

        public async Task<Vehicle> SaveVehicle(Vehicle vehicle)
        {
            await _unitOfWork.Vehicles.AddAsync(vehicle);
            await _unitOfWork.CompleteAsync();
            return vehicle;
        }

        public async Task UpdateVehicle(Vehicle vehicleToBeUpdated, Vehicle vehicle)
        {
            vehicleToBeUpdated.Model = vehicle.Model;
            vehicleToBeUpdated.Brand = vehicle.Model;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteVehicle(Vehicle vehicle)
        {
            _unitOfWork.Vehicles.Remove(vehicle);
            await _unitOfWork.CompleteAsync();
        }
    }
}