﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class FriendService : IFriendService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FriendService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Friend>> GetAllFriends()
        {
            return await _unitOfWork.Friends.GetAllFriendsAsync();
        }

        public async Task<IEnumerable<Friend>> GetFriendsByUserId(int userId)
        {
            return await _unitOfWork.Friends.GetFriendsByUserIdAsync(userId);
        }

        public async Task<Friend> GetFriendByUserIdAndFriendId(int userId, int friendId)
        {
            return await _unitOfWork.Friends.GetFriendByUserIdAndFriendId(userId, friendId);
        }


        public async Task<Friend> SaveFriend(Friend friend)
        {
            await _unitOfWork.Friends.AddAsync(friend);
            await _unitOfWork.CompleteAsync();
            return friend;
        }

        public async Task UpdateFriend(Friend friendToBeUpdated, Friend friend)
        {
            friendToBeUpdated.Comment = friend.Comment;
            friendToBeUpdated.NickName = friend.NickName;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteFriend(Friend friend)
        {
            _unitOfWork.Friends.Remove(friend);
            await _unitOfWork.CompleteAsync();
        }
    }
}