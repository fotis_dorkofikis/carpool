﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarPool.Core;
using CarPool.Core.Models;
using CarPool.Core.Services;

namespace CarPool.Services
{
    public class UserDataService : IUserDataService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserDataService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<UserData>> GetAllUsersData()
        {
            return await _unitOfWork.UsersData.GetAllUsersDataAsync();
        }

        public async Task<UserData> GetUserDataByUserId(int userId)
        {
            return await _unitOfWork.UsersData.GetUserDataByUserIdAsync(userId);
        }

        public async Task<UserData> SaveUserData(UserData userData)
        {
            await _unitOfWork.UsersData.AddAsync(userData);
            await _unitOfWork.CompleteAsync();
            return userData;
        }

        public async Task UpdateUserData(UserData userDataToBeUpdated, UserData userData)
        {
            userDataToBeUpdated.FirstName = userData.FirstName;
            userDataToBeUpdated.LastName = userData.LastName;
            userDataToBeUpdated.Email = userData.Email;
            userDataToBeUpdated.Phone = userData.Phone;
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteUserData(UserData userData)
        {
            _unitOfWork.UsersData.Remove(userData);
            await _unitOfWork.CompleteAsync();
        }
    }
}